

// Hàm vowelsAndConsonants()
function vowelsAndConsonants(s) {
  let t = '', u = '';
  for (let i = 0; i < s.length; i += 1) {
    switch (s.charAt(i)) {
      case 'a':
      case 'e':
      case 'i':
      case 'o':
      case 'u': {
        t += s.charAt(i);
        break;
      }
      default: {
        u += s.substr(i, 1);
      }
    }
  }
  t += u;
  return t;
}

console.log(vowelsAndConsonants('javascript'));
