// const dùng để khai báo biến có giá trị không đổi (hằng)
const a = 10;
const a1 = 'abc';

// let, var dùng để khai báo biến có thể thay đổi giá trị
// biến được khai báo bởi let chỉ sử dụng được trong 1 scope ('{}')
// biến được khai báo bởi var có thể sử dụng được kể cả ngoài 1 scope
function letVar() {
  for (var i = 0; i < 10; i += 1) {
    if (i == 5) console.log('Yeahh!!');
  }
  console.log(i);

  for (let j = 0; j < 5; j += 1) {
    if (j == 2) console.log('oh!!!!');
  }
}

letVar();
