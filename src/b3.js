
// Hàm tìm học lực
function timHocLuc(a) {
  if (a > 0 && a <= 25) return 'E';
  else if (a > 25 && a <= 40) return 'D';
  else if (a > 40 && a <= 65) return 'C';
  else if (a > 65 && a <= 80) return 'B';
  else return 'A';
}

console.log(timHocLuc(50));
